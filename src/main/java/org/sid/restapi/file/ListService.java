package org.sid.restapi.file;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("list")
public class ListService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getFilesList(){
		return Arrays.asList("path/subpath/file1.txt", "path/subpath/file2.txt");
	}
	
}
