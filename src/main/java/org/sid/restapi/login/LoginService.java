package org.sid.restapi.login;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.S3Object;

/**
 * Root resource (exposed at "login" path)
 */
@Path("doLogin")
public class LoginService {

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to
	 * the client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.TEXT_PLAIN)
	public boolean doLogin(@QueryParam("username") String username,
			@QueryParam("password") String password) {

//		BasicAWSCredentials awsCreds = new BasicAWSCredentials(username,
//				password);
//		AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
//				.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
//				.build();
//		S3Object obj = null;
//
//		try{
//			obj = s3Client.getObject("sajain-bkt-1", "license_LIVE.lic");
//		}catch(AmazonS3Exception ex){
//			ex.printStackTrace();
//		}
		
		return true;
	}

}
